﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System;
using System.Numerics;

namespace Dungeoff
{
    public abstract class Particle
    {
        private const float Friction = 20f;

        public TimeSpan Age { get; set; }
        public abstract Vector2 Position { get; set; }
        public abstract Vector2 Velocity { get; set; }

        protected Particle(Vector2 position, Vector2 velocity)
        {
            Age = TimeSpan.Zero;
            Position = position;
            Velocity = velocity;
        }

        public void Update(CanvasTimingInformation dt)
        {
            Age += dt.ElapsedTime;
            Position += Velocity;
            if (dt.ElapsedTime.TotalMilliseconds != 0f)
            {
                Velocity /= Friction / (float)dt.ElapsedTime.TotalMilliseconds;
            }
        }

        public abstract void Draw(CanvasDrawingSession ds);
    }
}