﻿using Microsoft.Graphics.Canvas;
using System;
using System.Numerics;
using Windows.UI;

namespace Dungeoff
{
    public class DamageIndicatorParticle : Particle
    {
        public int Damage { get; }

        public DamageIndicatorParticle(Vector2 position, int damage) : base(position, new Vector2(0f, -10f))
        {
            Damage = damage;
        }

        public override Vector2 Position { get; set; }
        public override Vector2 Velocity { get; set; }

        public override void Draw(CanvasDrawingSession ds)
        {
            using (ds.Push())
            {
                ds.Transform = Matrix3x2.CreateTranslation(Position) * ds.Transform;
                ds.DrawText(Damage.ToString(), Vector2.Zero, Colors.Red);
            }
        }
    }
}