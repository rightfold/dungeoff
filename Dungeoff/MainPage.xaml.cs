﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using Microsoft.Graphics.Canvas.UI.Xaml;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.System;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Dungeoff
{
    public sealed partial class MainPage : Page
    {
        private readonly World World;
        private readonly ISet<VirtualKey> Keys;

        public MainPage()
        {
            World = new World();
            Keys = new HashSet<VirtualKey>();
            InitializeComponent();
        }

        new private void Loaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Window.Current.CoreWindow.KeyDown += KeyDown;
            Window.Current.CoreWindow.KeyUp += KeyUp;
        }

        new private void Unloaded(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            Window.Current.CoreWindow.KeyDown -= KeyDown;
            Window.Current.CoreWindow.KeyUp -= KeyUp;
        }

        private void Update(ICanvasAnimatedControl sender, CanvasAnimatedUpdateEventArgs args)
        {
            World.Update(args.Timing, Keys);
        }

        private void Draw(ICanvasAnimatedControl sender, CanvasAnimatedDrawEventArgs args)
        {
            var ds = args.DrawingSession;
            ds.Antialiasing = CanvasAntialiasing.Aliased;
            World.Draw(ds, (float)sender.Size.Width, (float)sender.Size.Height);
        }

        new private void KeyDown(object sender, KeyEventArgs e)
        {
            Keys.Add(e.VirtualKey);
        }

        new private void KeyUp(object sender, KeyEventArgs e)
        {
            Keys.Remove(e.VirtualKey);
        }

        private void CreateResources(ICanvasAnimatedControl sender, CanvasCreateResourcesEventArgs args)
        {
            args.TrackAsyncAction(CreateResourcesAsync(sender).AsAsyncAction());
        }

        private async Task CreateResourcesAsync(ICanvasAnimatedControl sender)
        {
            Assets.Grass = await CanvasBitmap.LoadAsync(sender, "Assets/Grass.png");
            Assets.Pot = await CanvasBitmap.LoadAsync(sender, "Assets/Pot.png");
            Assets.Spider = await CanvasBitmap.LoadAsync(sender, "Assets/Spider.png");
            Assets.Stone = await CanvasBitmap.LoadAsync(sender, "Assets/Stone.png");
        }
    }
}