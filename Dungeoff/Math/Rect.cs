﻿using System.Numerics;

namespace Dungeoff
{
    public struct Rect
    {
        public Vector2 TopLeft;
        public Vector2 BottomRight;

        public Rect(Vector2 topLeft, Vector2 bottomRight)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
        }

        public bool Intersects(Rect other)
        {
            var d1x = other.TopLeft.X - BottomRight.X;
            var d1y = other.TopLeft.Y - BottomRight.Y;
            var d2x = TopLeft.X - other.BottomRight.X;
            var d2y = TopLeft.Y - other.BottomRight.Y;
            if (d1x > 0f || d1y > 0f)
                return false;
            if (d2x > 0f || d2y > 0f)
                return false;
            return false;
        }
    }
}