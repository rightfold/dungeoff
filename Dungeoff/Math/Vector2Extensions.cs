﻿using System;
using System.Numerics;

namespace Dungeoff
{
    public static class Vector2Extensions
    {
        public static Vector2 Unit(this Vector2 v)
        {
            return v / v.Length();
        }

        public static Vector2 Move(this Vector2 v, float direction, float amount)
        {
            v.X += amount * (float)Math.Cos(direction);
            v.Y += amount * (float)Math.Sin(direction);
            return v;
        }

        public static Vector2 Round(this Vector2 v)
        {
            return new Vector2((float)Math.Round(v.X), (float)Math.Round(v.Y));
        }
    }
}