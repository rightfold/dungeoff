﻿using Microsoft.Graphics.Canvas;

namespace Dungeoff
{
    public static class Assets
    {
        public static CanvasBitmap Grass;
        public static CanvasBitmap Pot;
        public static CanvasBitmap Spider;
        public static CanvasBitmap Stone;
    }
}