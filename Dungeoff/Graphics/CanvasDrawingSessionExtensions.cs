﻿using Microsoft.Graphics.Canvas;
using System;
using System.Numerics;

namespace Dungeoff
{
    public static class CanvasDrawingSessionExtensions
    {
        private sealed class PushDisposable : IDisposable
        {
            private readonly CanvasDrawingSession DS;
            private readonly Matrix3x2 Transform;

            public PushDisposable(CanvasDrawingSession ds)
            {
                DS = ds;
                Transform = ds.Transform;
            }

            public void Dispose()
            {
                DS.Transform = Transform;
            }
        }

        public static IDisposable Push(this CanvasDrawingSession ds)
        {
            return new PushDisposable(ds);
        }
    }
}