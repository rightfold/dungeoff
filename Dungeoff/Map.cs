﻿using Microsoft.Graphics.Canvas;
using System;
using System.Numerics;
using Windows.UI;

namespace Dungeoff
{
    public class Map
    {
        public enum TileType : ushort
        {
            Void,
            Grass,
            Stone,
            Tree,
            Sand,
        }

        public struct Tile
        {
            public TileType Type;
            public byte Variation;
            public bool IsFloor;
            public bool IsWall { get { return !IsFloor; } set { IsFloor = !value; } }

            public void Draw(CanvasDrawingSession ds, int x, int y)
            {
                using (ds.Push())
                {
                    ds.Transform = Matrix3x2.CreateTranslation(new Vector2(x * 16f, y * 16f)) * ds.Transform;
                    switch (Type)
                    {
                        case TileType.Void: break;
                        case TileType.Grass: ds.DrawImage(Assets.Grass, 0f, 0f); break;
                        case TileType.Stone: ds.DrawImage(Assets.Stone, 0f, 0f); break;
                        case TileType.Tree: break;
                        case TileType.Sand: break;
                    }
                }
            }
        }

        private Tile[,] Tiles;

        public Map()
        {
            var width = 100;
            var height = 100;
            Tiles = new Tile[width, height];

            var rnd = new Random();
            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    this[x, y] = new Tile { Type = TileType.Grass, IsFloor = true };
                }
            }

            for (var i = 0; i < Math.Sqrt(width * height) / 2; ++i)
            {
                var x = rnd.Next(0, width);
                var y = rnd.Next(0, height);
                var n = rnd.Next(10, 50);
                for (var t = 0; t < n; ++t)
                {
                    this[x, y] = new Tile
                    {
                        Type = TileType.Tree,
                        IsWall = true,
                    };
                    x += rnd.Next(-1, 2);
                    y += rnd.Next(-1, 2);
                }
            }

            for (var i = 0; i < Math.Sqrt(width * height) / 5; ++i)
            {
                var x = rnd.Next(0, width);
                var y = rnd.Next(0, height);
                var n = rnd.Next(250, 500);
                for (var t = 0; t < n; ++t)
                {
                    this[x, y] = new Tile
                    {
                        Type = TileType.Stone,
                        IsWall = true,
                    };
                    x += rnd.Next(-1, 2);
                    y += rnd.Next(-1, 2);
                }
            }
        }

        public Tile this[int x, int y]
        {
            get
            {
                if (x < 0 || x >= Tiles.GetLength(0) || y < 0 || y >= Tiles.GetLength(1))
                {
                    return new Tile { Type = TileType.Void, IsWall = true };
                }
                return Tiles[x, y];
            }
            set
            {
                if (x < 0 || x >= Tiles.GetLength(0) || y < 0 || y >= Tiles.GetLength(1))
                {
                    return;
                }
                Tiles[x, y] = value;
            }
        }

        public void Draw(CanvasDrawingSession ds)
        {
            var width = Tiles.GetLength(0);
            var height = Tiles.GetLength(1);
            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    this[x, y].Draw(ds, x, y);
                }
            }
        }
    }
}