﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Windows.System;

namespace Dungeoff
{
    public class World
    {
        public Map Map;
        private Player Player;
        public ISet<Entity> Entities;
        public ISet<Particle> Particles;

        public World()
        {
            Map = new Map();

            Entities = new HashSet<Entity>();

            Player = new Player(new Vector2(100f, 100f));
            Entities.Add(Player);

            var z1 = new Spider(new Vector2(100f, 150f), 2f);
            var z2 = new Zombie(new Vector2(150f, 100f), 3f);
            var z3 = new Zombie(new Vector2(150f, 150f), 4f);
            z1.Buff(Buff.MeleeRange, new TimeSpan(0, 1, 0));
            z2.Buff(Buff.Knockback, new TimeSpan(0, 1, 0));
            z3.Buff(Buff.Damage, new TimeSpan(0, 1, 0));
            Entities.Add(z1);
            Entities.Add(z2);
            Entities.Add(z3);

            Entities.Add(new Pot(new Vector2(200f, 200f)));

            Particles = new HashSet<Particle>();
        }

        public void Update(CanvasTimingInformation dt, ISet<VirtualKey> keys)
        {
            foreach (var m in Entities.OfType<Monster>().Where(m => m.Health <= 0).ToList())
            {
                Entities.Remove(m);
            }

            foreach (var p in Particles.Where(p => p.Age.TotalMilliseconds > 1000).ToList())
            {
                Particles.Remove(p);
            }

            foreach (var m in Entities)
            {
                if (m is Monster)
                {
                    (m as Monster).Update(dt, this);
                }
                else if (m is Player)
                {
                    (m as Player).Update(dt, keys, this);
                }
            }
            foreach (var p in Particles) p.Update(dt);
        }

        public void Draw(CanvasDrawingSession ds, float width, float height)
        {
            ds.Transform = Matrix3x2.CreateTranslation((-Player.Position + new Vector2(width / 2f, height / 2f)).Round());
            Map.Draw(ds);
            foreach (var e in Entities) e.Draw(ds);
            foreach (var p in Particles) p.Draw(ds);
        }

        public Player NearestPlayer(Vector2 xy)
        {
            var players = Entities.OfType<Player>().ToList();
            if (players.Count == 0)
            {
                return null;
            }
            else
            {
                return players.Aggregate((p, q) => (p.Position - xy).Length() < (q.Position - xy).Length() ? p : q);
            }
        }
    }
}