﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System;
using System.Numerics;

namespace Dungeoff
{
    public abstract class Entity
    {
        protected Entity(Vector2 position)
        {
            Position = position;
            Velocity = Vector2.Zero;
        }

        #region Movement

        public abstract Vector2 Position { get; protected set; }
        public abstract Vector2 Velocity { get; set; }
        public abstract Vector2 Friction { get; }
        public abstract Rect BoundingBox { get; }

        protected virtual void Update(CanvasTimingInformation dt, World w)
        {
            Move(dt, w);
        }

        protected virtual void Move(CanvasTimingInformation dt, World w)
        {
            if (Velocity == Vector2.Zero)
            {
                return;
            }

            var boundingBox = BoundingBox;
            boundingBox.TopLeft += Velocity;
            boundingBox.BottomRight += Velocity;
            int leftTile = (int)Math.Floor(boundingBox.TopLeft.X / 16f);
            int rightTile = (int)Math.Ceiling(boundingBox.BottomRight.X / 16f) - 1;
            int topTile = (int)Math.Floor(boundingBox.TopLeft.Y / 16f);
            int bottomTile = (int)Math.Ceiling(boundingBox.BottomRight.Y / 16f) - 1;
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    if (w.Map[x, y].IsWall)
                    {
                        Velocity = Vector2.Zero;
                        goto endCollisionDetect;
                    }
                }
            }
        endCollisionDetect:

            Position += Velocity;
            if (dt.ElapsedTime.TotalMilliseconds != 0f)
            {
                Velocity /= Friction / (float)dt.ElapsedTime.TotalMilliseconds;
            }
        }

        #endregion Movement

        public abstract void Harm(World w, int damage);

        public abstract void Draw(CanvasDrawingSession ds);
    }
}