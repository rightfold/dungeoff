﻿namespace Dungeoff
{
    public class Buff
    {
        public float MeleeRangeFactor { get; protected set; } = 1f;
        public float KnockbackFactor { get; protected set; } = 1f;
        public float DamageFactor { get; protected set; } = 1f;

        public static Buff MeleeRange = new Buff() { MeleeRangeFactor = 1.5f };
        public static Buff Knockback = new Buff() { KnockbackFactor = 1.5f };
        public static Buff Damage = new Buff() { DamageFactor = 1.5f };
    }
}