﻿using Microsoft.Graphics.Canvas.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Dungeoff
{
    public abstract class Character : Entity
    {
        protected Character(Vector2 position, int health) : base(position)
        {
            Buffs = new Dictionary<Buff, TimeSpan>();
            Health = health;
        }

        protected override void Update(CanvasTimingInformation dt, World w)
        {
            AgeBuffs(dt);
            base.Update(dt, w);
        }

        #region Buffs

        private readonly Dictionary<Buff, TimeSpan> Buffs;

        public void Buff(Buff buff, TimeSpan timeSpan)
        {
            if (Buffs.ContainsKey(buff))
            {
                Buffs[buff] += timeSpan;
            }
            else
            {
                Buffs.Add(buff, timeSpan);
            }
        }

        private void AgeBuffs(CanvasTimingInformation dt)
        {
            foreach (var buff in Buffs.Keys.ToList())
            {
                Buffs[buff] -= dt.ElapsedTime;
                if (Buffs[buff] < TimeSpan.Zero)
                {
                    Buffs.Remove(buff);
                }
            }
        }

        #endregion Buffs

        #region Health

        public int Health { get; protected set; }

        public override void Harm(World w, int damage)
        {
            w.Particles.Add(new DamageIndicatorParticle(Position, damage));
            Health -= damage;
        }

        #endregion Health

        #region Stats

        public abstract float BaseMeleeRange { get; }
        public abstract float BaseKnockback { get; }
        public abstract int BaseDamage { get; }

        public float MeleeRange => Buffs.Keys.Aggregate(BaseMeleeRange, (r, b) => r * b.MeleeRangeFactor);
        public float Knockback => Buffs.Keys.Aggregate(BaseKnockback, (r, b) => r * b.KnockbackFactor);
        public int Damage => (int)Buffs.Keys.Aggregate((float)BaseDamage, (r, b) => r * b.DamageFactor);

        #endregion Stats

        protected void MeleeAttack(World w, Entity target)
        {
            var distance = (target.Position - Position).Length();
            if (distance < MeleeRange)
            {
                target.Velocity += (target.Position - Position).Unit() * Knockback;
                target.Harm(w, Damage);
            }
        }
    }
}