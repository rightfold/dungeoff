﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Windows.System;
using Windows.UI;

namespace Dungeoff
{
    public class Player : Character
    {
        private TimeSpan Age;
        private TimeSpan LastAttack;

        public Player(Vector2 position) : base(position, 100)
        {
            Age = TimeSpan.Zero;
            LastAttack = TimeSpan.Zero;
        }

        public override Vector2 Position { get; protected set; }
        public override Vector2 Velocity { get; set; }
        public override Vector2 Friction => new Vector2(30f, 30f);
        public override Rect BoundingBox => new Rect(Position - new Vector2(8f), Position + new Vector2(8f));

        public override float BaseMeleeRange => 32f;
        public override float BaseKnockback => 32f;
        public override int BaseDamage => 30;

        public void Update(CanvasTimingInformation dt, ISet<VirtualKey> keys, World w)
        {
            Age += dt.ElapsedTime;
            Attack(dt, keys, w);
            Walk(dt, keys, w);
            base.Update(dt, w);
        }

        private void Attack(CanvasTimingInformation dt, ISet<VirtualKey> keys, World w)
        {
            if (Age.TotalMilliseconds - LastAttack.TotalMilliseconds > 1000f)
            {
                if (keys.Contains(VirtualKey.Space))
                {
                    foreach (var target in w.Entities)
                    {
                        if (target != this)
                        {
                            MeleeAttack(w, target);
                        }
                    }
                    LastAttack = dt.TotalTime;
                }
            }
        }

        private void Walk(CanvasTimingInformation dt, ISet<VirtualKey> keys, World w)
        {
            if (keys.Contains(VirtualKey.W))
            {
                var v = Velocity;
                v.Y -= (float)dt.ElapsedTime.TotalMilliseconds / 10f;
                Velocity = v;
            }
            if (keys.Contains(VirtualKey.S))
            {
                var v = Velocity;
                v.Y += (float)dt.ElapsedTime.TotalMilliseconds / 10f;
                Velocity = v;
            }
            if (keys.Contains(VirtualKey.A))
            {
                var v = Velocity;
                v.X -= (float)dt.ElapsedTime.TotalMilliseconds / 10f;
                Velocity = v;
            }
            if (keys.Contains(VirtualKey.D))
            {
                var v = Velocity;
                v.X += (float)dt.ElapsedTime.TotalMilliseconds / 10f;
                Velocity = v;
            }
        }

        public override void Draw(CanvasDrawingSession ds)
        {
            using (ds.Push())
            {
                ds.Transform = Matrix3x2.CreateTranslation(Position) * ds.Transform;
                ds.DrawRectangle(-8f, -8f, 16f, 16f, Colors.White);
            }
        }
    }
}