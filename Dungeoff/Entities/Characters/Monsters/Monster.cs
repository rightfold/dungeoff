﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System.Numerics;

namespace Dungeoff
{
    public abstract class Monster : Character
    {
        protected Monster(Vector2 position, int health) : base(position, health)
        {
        }

        protected virtual Player Target(World w)
        {
            return w.NearestPlayer(Position);
        }

        new public virtual void Update(CanvasTimingInformation dt, World w)
        {
            base.Update(dt, w);
        }
    }
}