﻿using Microsoft.Graphics.Canvas;
using Microsoft.Graphics.Canvas.UI;
using System;
using System.Numerics;
using Windows.UI;

namespace Dungeoff
{
    public class Zombie : Monster
    {
        private float Direction;

        public Zombie(Vector2 position, float direction) : base(position, 100)
        {
            Direction = direction;
        }

        public override Vector2 Position { get; protected set; }
        public override Vector2 Velocity { get; set; }
        public override Vector2 Friction => new Vector2(30f, 30f);
        public override Rect BoundingBox => new Rect(Position - new Vector2(8f), Position + new Vector2(8f));

        public override float BaseMeleeRange => 16f;
        public override float BaseKnockback => 16f;
        public override int BaseDamage => 10;

        public override void Update(CanvasTimingInformation dt, World w)
        {
            MeleeAttack(w, Target(w));
            ApproachTarget(dt, w);
            base.Update(dt, w);
        }

        private void ApproachTarget(CanvasTimingInformation dt, World w)
        {
            var target = Target(w);
            Direction = (float)Math.Atan2(target.Position.Y - Position.Y, target.Position.X - Position.X);
            Velocity = Velocity.Move(Direction, (float)dt.ElapsedTime.TotalMilliseconds / 60f);
        }

        public override void Draw(CanvasDrawingSession ds)
        {
            using (ds.Push())
            {
                ds.Transform = Matrix3x2.CreateTranslation(Position) * ds.Transform;
                ds.DrawRectangle(-8f, -16f, (float)Health / 100f * 16f, 2f, Colors.Black);
            }
            using (ds.Push())
            {
                ds.Transform = Matrix3x2.CreateRotation(Direction) * Matrix3x2.CreateTranslation(Position) * ds.Transform;
                ds.DrawRectangle(-8f, -8f, 16f, 16f, Colors.Black);
            }
        }
    }
}