﻿using Microsoft.Graphics.Canvas;
using System.Numerics;
using Windows.UI;

namespace Dungeoff
{
    public class Pot : Entity
    {
        public override Vector2 Position { get; protected set; }
        public override Vector2 Velocity { get { return Vector2.Zero; } set { } }
        public override Vector2 Friction => Vector2.Zero;
        public override Rect BoundingBox => new Rect(Position - new Vector2(7f), Position + new Vector2(7f));

        public Pot(Vector2 position) : base(position)
        {
        }

        public override void Harm(World w, int damage)
        {
            w.Particles.Add(new DamageIndicatorParticle(Position, damage));
        }

        public override void Draw(CanvasDrawingSession ds)
        {
            using (ds.Push())
            {
                ds.Transform = Matrix3x2.CreateTranslation(Position) * ds.Transform;
                ds.DrawImage(Assets.Pot, -7f, -7f);
            }
        }
    }
}